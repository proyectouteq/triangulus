const piramide = (parametro) => {
    this.altura = parametro;
    for (var w = 1; this.altura >= w; w++) {
        for (var z = this.altura; z > w; z--) {
            process.stdout.write(' ');
        }
        for (var i = 0; i < w; i++) {
            //console.log('i: ',i ,'w: ',w)
            //i % 2  == 0 ? process.stdout.write(' '): null;
            process.stdout.write('* ');
        }
        console.log(' ');
    }
    for (var w = 1; this.altura >= w; w++) {
        for (var z = 0; z < w; z++) {
            process.stdout.write(' ');
        }
        for (var i = this.altura; i > w; i--) {
            //console.log('i: ',i ,'w: ',w)
            //i % 2  == 0 ? process.stdout.write(' '): null;
            process.stdout.write('* ');
        }
        console.log(' ');
    }
}

module.exports = {
    piramide
}