# Foobar

Esta es una librería de JavaScript para hacer triángulos en consola.

## Installation

Para la instalación utiliza npm

```bash
npm i triangulus
```

## Usage

```python
const {piramide} = require('triangulus')

# returns 
    *    
   * *   
  * * *  
 * * * *  
* * * * *  
 * * * *   
  * * *    
   * *     
    * 
piramide(5);

```

## Contributing
Se aceptan sugerencias :b

## License
[MIT](https://choosealicense.com/licenses/mit/)